
/**
* @param originList - list of numbers 
* @param currentSum - current sum of numbers which processes now 
* @param limitNum - sum of numbers combinations cannot be more of this value
 	 
**/
function Combinations() { 
 	
 	var closestValue = 0,
 		limitNum = 100

 	var getInnerCombinations = function(curList, currentSum) {
 		var possCombinations = [];
 		var me = this;

 		for (var i = 0, oLen = curList.length; i< oLen; i++) {
			var tempNum = curList[i];
			 		
			//игнорировать числа большие лимита
			if (tempNum > limitNum) continue;
				
			if (currentSum + tempNum < limitNum) {
				//можно еще комбинировать - т.к. сумма не превысила	limit
				//поиск для текущей суммы возможные дополнения, 
				// массив обрезать - без учета текущего значения(ий)
				var innerCombs = getInnerCombinations(curList.slice(i + 1), currentSum + tempNum, limitNum);
				if (innerCombs.length) {
					//слить комбинации с числом tempNum
					for (var j = 0, innerL = innerCombs.length; j < innerL; j++ ) {
						possCombinations.push([tempNum].concat(innerCombs[j]));
					}
				}
				else {
					possCombinations.push([tempNum, currentSum + tempNum]);
				}
			}
			else if (currentSum + tempNum == limitNum){
				possCombinations.push([tempNum, currentSum + tempNum]);
			} 
			
			if (currentSum + tempNum <=limitNum && currentSum + tempNum > closestValue) {
				closestValue = currentSum + tempNum;
			}
		}
		return possCombinations;
 	}

 	var getClosest =  function (originList, limit) {
 		var me = this;
 		closestValue = 0;
 		limitNum = limit;
 		debugger;
 		//поиск возможных комбинаций
 		var combs = getInnerCombinations (originList, 0);
 		combs = combs.filter(function(item){return item[item.length-1]==closestValue;});

 		return combs.map(function(item){return item.slice(0, item.length-1);});
 	};

 	return {
 		//Object public API
		getClosest: getClosest 		
 	} 
};



var testCases = [[45, 66, 12, 18, 30, 20, 15], 
				 [6,34, 4, 2, 6, 3,22, 12],
  				 [95,1, 1 ,9,80, 3, 2]];

for (var i = 0; i < testCases.length; i++ ) {
	console.log('Array of natural numbers: ', testCases[i]);
	console.log('Combination with sum closest to 100: ', new Combinations().getClosest(testCases[i], 100));
}


