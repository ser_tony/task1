
/**
При решения такой задачи можно применить некоторые native возможности javascript: setTimeout, clearTimeout, apply - с передачей контекста и параметров-массива аргументов (arguments) в   замыкание - в котором сохраняются переменные context, args, result, объект timeout и функция later - которая  будет выполняться  по setTimeout в случае, если оставшееся время "remaining" - разница между предыдущим вызовом целевой(клиентской) функции и текущим моментом времени не удовлетворяет условию: (remaining <= 0 || remaining > wait). Здесь wait - время в течении которого будет  ограничено выполнение функции.   Общий алгоритм следующий:  
*/  

 throttle = function(func, wait) {
    var context, args, result;
   //объект - сохраняющий timeout 
   var timeout = null;
    //предыдущее значение метки времени
    var previous = 0;
    
    //обертка(wrapper) над целевой функцией 
    var later = function() {
      previous =  0;
      timeout = null;
      result = func.apply(context, args);
      if (!timeout) context = args = null;
    };
    return function() {
      var now = Date.now();
      if (!previous) previous = now;
      //вычисление нового значение заключенное в замыкании функции
      var remaining = wait - (now - previous);
      context = this;
      args = arguments;
      if (remaining <= 0 || remaining > wait) {
        clearTimeout(timeout);
        timeout = null;
        previous = now;
        result = func.apply(context, args);
        if (!timeout) context = args = null;
      } else if (!timeout) {
        timeout = setTimeout(later, remaining);
      }
      return result;
    };
  };

/**
 Тогда, чтобы клиентская функция выполнялась с частотой N раз в секунду можно использовать функцию throttle: 
   ind = 0; var clientFun =  function(){console.log(ind++);}  
   N = 2;
   readyFun = throttle(clientFun, 1000/N); 
*/


